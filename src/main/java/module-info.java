module Mappe2 {
    requires javafx.controls;
    requires javafx.fxml;
    requires opencsv;

    exports mappe.del2.GUI.controllers to javafx.fxml;
    opens mappe.del2.GUI.controllers to javafx.fxml;
    opens mappe.del2.hospital to javafx.base;

    opens mappe.del2.GUI;
}