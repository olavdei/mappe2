package mappe.del2.GUI.controllers;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javafx.stage.Window;
import mappe.del2.factory.DeleteConfirmation;
import mappe.del2.factory.GuiWindow;
import mappe.del2.factory.NewPatientDialog;
import mappe.del2.factory.SceneFactory;
import mappe.del2.hospital.Pasient;
import mappe.del2.persistence.CSVManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class PatientRegister implements Initializable {
    private ObservableList<Pasient> people;

    @FXML private TableView<Pasient> tableView;
    @FXML private TableColumn<Pasient, String> firstNameColumn;
    @FXML private TableColumn<Pasient, String> lastNameColumn;
    @FXML private TableColumn<Pasient, String> ssnColumn;


    public void importFromCSV(ActionEvent actionEvent) {
        FileChooser fc = new FileChooser();

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Pasient, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Pasient, String>("lastName"));
        ssnColumn.setCellValueFactory(new PropertyValueFactory<Pasient, String>("socialSecurityNumber"));

        String path = fc.showOpenDialog(null).getPath();

        try {
            tableView.setItems(getPeopleCustom(path));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void exportToCSV(ActionEvent actionEvent) {
        System.out.println("export");
    }

    public void quit(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void addPatient(ActionEvent actionEvent) {
        Pasient newPatient = null;
        try {
            newPatient = new Pasient("", "", "", "12345678910");
        }catch (Exception e){
            e.printStackTrace();
        }

        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);

        if (actionEvent.getSource() instanceof Node){
            final Node source = (Node) actionEvent.getSource();
            final Stage sourceStage = (Stage) source.getScene().getWindow();
            stage.setX(sourceStage.getX() + 50);
            stage.setY(sourceStage.getY());
        }
        GuiWindow window = factory.getScene("newPatient");


        try {
            stage.setScene(window.loadScene());
            if (window instanceof NewPatientDialog){
                NewPatientDetails controller = ((NewPatientDialog) window).getController();
                controller.initData(newPatient, this);
            }
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void editSelectedPatient(ActionEvent actionEvent) {
        Pasient patient = tableView.getSelectionModel().getSelectedItem();

        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);

        if (actionEvent.getSource() instanceof Node){
            final Node source = (Node) actionEvent.getSource();
            final Stage sourceStage = (Stage) source.getScene().getWindow();
            stage.setX(sourceStage.getX() + 50);
            stage.setY(sourceStage.getY());
        }
        GuiWindow window = factory.getScene("newPatient");


        try {
            stage.setScene(window.loadScene());
            if (window instanceof NewPatientDialog){
                NewPatientDetails controller = ((NewPatientDialog) window).getController();
                controller.initData(patient, this);
            }
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void removeSelectedPatient(ActionEvent actionEvent) {
        Pasient patient = tableView.getSelectionModel().getSelectedItem();

        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);

        if (actionEvent.getSource() instanceof Node){
            final Node source = (Node) actionEvent.getSource();
            final Stage sourceStage = (Stage) source.getScene().getWindow();
            stage.setX(sourceStage.getX() + 50);
            stage.setY(sourceStage.getY());
        }

        GuiWindow window = factory.getScene("deleteConfirmation");

        try {
            stage.setScene(window.loadScene());
            if (window instanceof DeleteConfirmation){
                DeleteConfirmationController controller = ((DeleteConfirmation) window).getController();
                controller.initData(patient, this);
            }
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void aboutWindow(ActionEvent actionEvent) {
        Stage stage = newWindow("informationDialog", actionEvent);

        try {
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Stage newWindow(String factoryString, ActionEvent event){
        SceneFactory factory = new SceneFactory();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);

        if (event.getSource() instanceof Node){
            final Node source = (Node) event.getSource();
            final Stage sourceStage = (Stage) source.getScene().getWindow();
            stage.setX(sourceStage.getX() + 50);
            stage.setY(sourceStage.getY());
        }

        try {
            stage.setScene(factory.getScene(factoryString).loadScene());
            return stage;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Pasient, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Pasient, String>("lastName"));
        ssnColumn.setCellValueFactory(new PropertyValueFactory<Pasient, String>("socialSecurityNumber"));



        try {
            getPeople();
            tableView.setItems(people);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void refresh(Pasient updatedPatient){
        if (!(people.contains(updatedPatient))){
            people.add(updatedPatient);
        }
        tableView.setItems(people);
        tableView.refresh();
    }

    public void getPeople() throws IOException {
        CSVManager csvManager = new CSVManager();
        people = csvManager.readFile();
    }

    public void removePatient(Pasient p){
        people.remove(p);
        tableView.setItems(people);
        tableView.refresh();
    }

    public ObservableList<Pasient> getPeopleCustom(String path) throws IOException {
        CSVManager csvManager = new CSVManager();
        return csvManager.readFileCustomPath(path);
    }
}
