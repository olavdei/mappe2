package mappe.del2.GUI.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mappe.del2.hospital.Pasient;

import java.net.URL;
import java.util.ResourceBundle;

public class NewPatientDetails implements Initializable {

    private Pasient patient;
    private PatientRegister controller;

    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML private TextField ssnTextField;


    public void initData(Pasient patient, PatientRegister controller){
        this.controller = controller;

        if (patient != null) {
            this.patient = patient;

            firstNameField.setText(this.patient.getFirstName());
            lastNameField.setText(this.patient.getLastName());
            ssnTextField.setText(this.patient.getSocialSecurityNumber());
        }
    }

    public void addPatient(ActionEvent actionEvent) {
        patient.setFirstName(firstNameField.getText());
        patient.setLastName(lastNameField.getText());
        patient.setSocialSecurityNumber(ssnTextField.getText());
        controller.refresh(patient);

        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();

        stage.close();
    }

    public void closeWindow(ActionEvent actionEvent) {
        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();

        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
