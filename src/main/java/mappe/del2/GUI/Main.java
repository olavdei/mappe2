package mappe.del2.GUI;

import com.opencsv.CSVReader;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mappe.del2.factory.GuiWindow;
import mappe.del2.factory.SceneFactory;
import mappe.del2.hospital.Pasient;

import java.io.FileReader;

import java.util.ArrayList;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {

        try{
            SceneFactory sf = new SceneFactory();

            GuiWindow window = sf.getScene("patientRegister");
            Scene scene = window.loadScene();


            stage.setScene(scene);
            stage.show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
