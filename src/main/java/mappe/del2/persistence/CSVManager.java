package mappe.del2.persistence;

import com.opencsv.CSVReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import mappe.del2.hospital.Pasient;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVManager {
    String fileName;
    String filePath;
    ObservableList<Pasient> patientList;

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public ObservableList<Pasient> getPatientList() {
        return patientList;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setPatientList(ObservableList<Pasient> patientList) {
        this.patientList = patientList;
    }

    public CSVManager(String fileName, String filePath){
        this.fileName = fileName;
        this.filePath = filePath;

        this.patientList = FXCollections.observableArrayList();
    }
    public CSVManager(){
        this.filePath = "src/main/resources";
        this.fileName = "PatientTestData";

        this.patientList = FXCollections.observableArrayList();
    }

    public ObservableList<Pasient> readFile() throws IOException {
        FileReader reader = new FileReader(this.filePath + "/" + this.fileName + ".csv");
        this.patientList = FXCollections.observableArrayList();

        CSVReader csvReader = new CSVReader(reader);
        try{
            String[] nextLine;

            while ((nextLine = csvReader.readNext()) != null){
                for (var e: nextLine){
                    String[] patient = e.split(";");
                    try {
                        this.patientList.add(new Pasient(patient[0], patient[1], patient[2], patient[3]));

                    }catch (Exception e1){
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        reader.close();
        return patientList;
    }
    public ObservableList<Pasient> readFileCustomPath(String path) throws IOException {
        FileReader reader = new FileReader(path);
        this.patientList = FXCollections.observableArrayList();

        CSVReader csvReader = new CSVReader(reader);
        try{
            String[] nextLine;

            while ((nextLine = csvReader.readNext()) != null){
                for (var e: nextLine){
                    String[] patient = e.split(";");
                    try {
                        this.patientList.add(new Pasient(patient[0], patient[1], patient[2], patient[3]));

                    }catch (Exception e1){
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        reader.close();
        return patientList;
    }

}
