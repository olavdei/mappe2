package mappe.del2.factory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class InformationDialog implements GuiWindow{
    @Override
    public Scene loadScene() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/InformationDialog.fxml"));

        AnchorPane root = loader.load();
        Scene scene = new Scene(root);

        return scene;    }
}
