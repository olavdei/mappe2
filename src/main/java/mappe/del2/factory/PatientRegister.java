package mappe.del2.factory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import mappe.del2.hospital.Pasient;
import mappe.del2.persistence.CSVManager;

import java.io.IOException;

public class PatientRegister implements GuiWindow{
    @Override
    public Scene loadScene() throws IOException {
        CSVManager csvManager = new CSVManager();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/PatientRegister.fxml"));

        AnchorPane root = loader.load();

        Scene scene = new Scene(root);

        return scene;
    }
}
