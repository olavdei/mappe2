package mappe.del2.factory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import mappe.del2.GUI.controllers.NewPatientDetails;

import java.io.IOException;

public class NewPatientDialog implements GuiWindow{
    private FXMLLoader loader;
    @Override
    public Scene loadScene() throws IOException {
        this.loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/NewPatientDetails.fxml"));

        AnchorPane root = loader.load();
        Scene scene = new Scene(root);

        return scene;
    }
    public NewPatientDetails getController() throws IOException {
        if (this.loader != null) {
            return this.loader.getController();
        }
        loadScene();
        return this.loader.getController();
    }
}
