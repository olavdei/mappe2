package mappe.del2.factory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import java.io.IOException;

public interface GuiWindow {
    Scene loadScene() throws IOException;

}
